# instahelper-loki

+ Обоснование выбора Loki в качестве системы управления логами: https://docs.google.com/document/d/1pEf38sXxFhAAHIH8PiPYuCrNLvr_vr95OZEKmIg5OHw/edit?usp=sharing

###Установка Loki

~~~shell
cd loki
ansible-playbook install_loki_playbook.yaml
~~~

### Установка promtail на агенты(для сбора логов из директоририи var/log инстансов)
~~~commandline
cd promtail-agents
ansible-playbook install_promtail_to_instances_playbook.yml
~~~

### Настройка контейнеров на push своих логов в loki по-умолчанию
+ Используется плагин _loki_ для докера 
~~~commandline
cd update-docker-logs
ansible-playbook update_docker_for_loki.yml
~~~