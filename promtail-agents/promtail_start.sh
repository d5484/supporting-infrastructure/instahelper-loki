#!/bin/bash

/usr/bin/docker run \
--detach \
--name=promtail \
--volume=/usr/local/configs:/mnt/config \
--volume=/var/log:/var/log \
--publish=9080:9080 \
grafana/promtail:latest \
--config.file=/mnt/config/promtail-config.yaml