#!/bin/bash

/usr/bin/docker run \
--detach \
--name=loki \
--volume=/usr/local/configs:/mnt/config \
--publish=2095:3100 \
grafana/loki:latest \
--config.file=/mnt/config/loki-config.yaml